'''
http://www.channel4000.com/inergizecnnvideo.xml

scrapes the channel4000 feed on a specified interval(if you run it in cron)

if they arent in upLynk (either in pending cloud job or in CMS), we create cloud slicing jobs for them
and pick them up next time around

'''

import urllib2
import urllib
import zlib
import hmac
import hashlib
import json
import time
import datetime
import sys
from HTMLParser import HTMLParser

import feedparser

class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.fed = []
    def handle_data(self, d):
        self.fed.append(d)
    def get_data(self):
        return ''.join(self.fed)

def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data().replace('\n','')

API_ROOT_URL = 'http://services.uplynk.com/api2'
OWNER = ''  # [your owner id]
SECRET = ''  # [your secret api key]

FEED_URL = 'http://www.channel4000.com/inergizecnnvideo.xml'


def call_uplynk(uri, **msg):
    msg['_owner'] = OWNER
    msg['_timestamp'] = int(time.time())
    msg = json.dumps(msg)
    msg = zlib.compress(msg, 9).encode('base64').strip()
    sig = hmac.new(SECRET, msg, hashlib.sha256).hexdigest()
    body = urllib.urlencode(dict(msg=msg, sig=sig))
    resp = json.loads(urllib2.urlopen(API_ROOT_URL + uri, body).read())
    return resp


def log(*args):
    try:
        line =  time.strftime('%y-%m-%d %H:%M:%S  ') + ' '.join(str(x) for x in args) + '\n'
        sys.stderr.write(line)
        sys.stderr.flush()
    except:
        print 'log:', repr(args)

class Scraper(object):
    def __init__(self):
        self.xml_feed = get_xml_feed()
        self.jobs = get_cloudslicer_jobs()


    def scrape(self):
        # send stuff thats not there to the cloud slicer
        log('*** PARSING FEED FOR UPDATES TO SEND TO SLICER ***')
        jobextids = [j.get('args').get('external_id') for j in get_cloudslicer_jobs()]
        for f in self.xml_feed:
            cmsId = f.id
            inCms=call_uplynk('/asset/get', external_id=cmsId).get('asset')
            if cmsId not in jobextids and not inCms:
                args=dict()
                _meta = []
                _meta.append('link={0}'.format(f.link))
                _meta.append('guid={0}'.format(f.id))
                #attempt to cleanup the summary
                _summary = strip_tags(f.summary)
                _meta.append('summary={0}'.format(_summary))

                _meta.append('pubDate={0}'.format(f.updated))
                args.update(meta=',,,'.join(_meta))
                args.update(description=f.title, skip_drm=1, external_id=f.id)
                # find the actual url of the asset
                url = ""
                highest = 0
                for item in f.media_content:
                    if float(item['bitrate']) > highest:
                        highest = float(item['bitrate'])
                        url = item['url']

                log('FOUND new asset! Sending {0} to cloudslicer'.format(f.id))
                create_cloudslicer_job(url, **args)
            else:
                log('{0} exists or is already queued'.format(f.id))
                
        cleanup_cloudslicer_jobs()

def cleanup_cloudslicer_jobs():
    cloudslicerjobs = get_cloudslicer_jobs()
    log('CLOUDSLICER - {0} jobs still pending'.format(len(cloudslicerjobs)))
    doneIds = [j.get('id') for j in cloudslicerjobs if j.get('state') == 'done']
    if doneIds:
        call_uplynk('/cloudslicer/jobs/delete', ids=doneIds)
        log('*** CLEANING UP OLD SLICER JOBS ***')


def create_cloudslicer_job(url, **kwargs):
    call_uplynk('/cloudslicer/jobs/create', args=kwargs, source=dict(url=url))


def get_cloudslicer_jobs(**kwargs):
    return call_uplynk('/cloudslicer/jobs/list', **kwargs).get('jobs')


def get_xml_feed():
    data = feedparser.parse(FEED_URL)
    feed = data.entries
    return feed

scraper=Scraper()
log('Scraping started at {0}'.format(datetime.datetime.now()))

scraper.scrape()
log('Done Scraping!')